import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProductComponent } from './product/product.component';
const routes: Routes = [
  // {path: "",component:AppComponent ,pathMatch:"full" },
  {path: "product",component:ProductComponent }
  // {path: "**",component:PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const Routing = [ProductComponent,PageNotFoundComponent,AppComponent];